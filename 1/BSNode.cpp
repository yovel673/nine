#include "BSNode.h"
#include <iostream>



BSNode::BSNode(string data) : _data(data), _right(NULL), _left(NULL), _count(1) {}


BSNode::BSNode(const BSNode & other) : _data(other.getData()), _right(other.getRight()), _left(other.getRight()) {}


BSNode::~BSNode()
{
	if (this != nullptr)
	{
		if (this->getLeft() != nullptr)
			delete this->getLeft();
		if (this->getRight() != nullptr)
			delete this->getRight();
	}
	else
		delete this;
}


void BSNode::insert(string value)
{
	this->search(value);
	if (value < this->getData())
		if (this->getLeft() == NULL)
			this->_left = new BSNode(value);
		else
			this->getLeft()->insert(value);
	else
		if (this->getRight() == NULL)
			this->_right = new BSNode(value);
		else
			this->getRight()->insert(value);

}


BSNode & BSNode::operator=(const BSNode & other)
{
	this->_data = other.getData();
	this->_right = other.getRight();
	this->_left = other.getRight();
	return *this;
}



bool BSNode::isLeaf() const
{
	return !(this->getLeft() || this->getRight());
}


string BSNode::getData() const
{
	return this->_data;
}


BSNode * BSNode::getLeft() const
{
	return this->_left;
}



BSNode * BSNode::getRight() const
{
	return this->_right;
}


bool BSNode::search(string val) const
{
	if (this->_data.compare(val) == 0)
	{
		return true;
	}
	if (this->_data > val && this->_left != nullptr)
	{
		return this->_left->search(val);
	}
	if (this->_data < val && this->_right != nullptr)
	{
		return this->_right->search(val);
	}
	return false;
}


int BSNode::getHeight() const
{
	int heightLeft = 0;
	int heightRight = 0;
	if (this->_left)
	{
		heightLeft = this->_left->getHeight();
	}
	if (this->_right)
	{
		heightRight = this->_right->getHeight();
	}
	if (this->isLeaf())
	{
		return 0;
	}
	if (heightLeft > heightRight)
	{
		return (heightLeft + 1);
	}
	return (heightRight + 1);
}


int BSNode::getDepth(const BSNode & root) const
{
	if (!(&root)->search(this->_data))
	{
		return -1;
	}
	if ((&root)->_data == this->_data)
	{
		return 0;
	}
	if ((&root)->_data > this->_data)
	{
		return 1 + this->getDepth(*((&root)->_left));
	}
	if ((&root)->_data < this->_data)
	{
		return 1 + this->getDepth(*((&root)->_right));
	}
	return -1;
}


void BSNode::printNodes() const
{

}
