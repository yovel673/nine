#include "printTreeToFile.h"

void printTreeToFile(const BSNode * bs, std::string output)
{
	FILE* file;
	fopen_s(&file, output.c_str(), "w");
	if (bs)
		fprintf(file, "%s", printNodesToFile(bs).c_str());
	fclose(file);
}

std::string printNodesToFile(const BSNode * bs)
{
	std::string str = "";
	str = bs->getData();
	if (bs->getLeft())
		str += " " + printNodesToFile(bs->getLeft()) + " #";
	else
		str += " #";
	if(bs->getRight())
		str += " " + printNodesToFile(bs->getRight());
	return str;
}
