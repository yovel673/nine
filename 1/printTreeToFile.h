#pragma once

#include <string>
#include "BSNode.h"
void printTreeToFile(const BSNode* bs, std::string output);
std::string printNodesToFile(const BSNode* bs);
