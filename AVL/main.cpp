#include "AVLTree.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"

using namespace std;

int main()
{
	AVLTree<int>* bs = new AVLTree<int>(6);
	bs->insert(7);
	bs->insert(8);
	bs->insert(0);
	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);
	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	return 0;
}

