#pragma once
#include "BSNode.h"

template<class T>
class AVLTree : public BSNode<T>
{
public:
	AVLTree(T data) : BSNode<T>(data)
	{
	}
	BSNode<T>* insert(T value) {
		
		if (value < this->_data)
		{
			if (this->_left) 
			{
				this->_left->insert(value); 
			}
			else
			{
				this->_left = new AVLTree<T>(value); 
				this->_left->setParent(this);
			}
		}
		else if (value> this->_data)
		{
			if (this->_right) // if there is already right son
			{
				this->_right->insert(value); // recursive call on the right son
			}
			else
			{
				this->_right = new AVLTree<T>(value); // add the node as right son
				this->_right->setParent(this);
			}
		}
		else //value == this->_data
		{
			throw 1;
		}

		int balance = this->getBalanceFactor();

		//Left Left Case
		if (balance > 1 && this->_data < this->_left->getData()) {
			return this->rotateRight();
		}
		//Right Right Case
		if (balance < -1 && this->_data > this->_right->getData()) {
			return this->rotateLeft();
		}
		//Left Right Case
		if (balance > 1 && this->_data > this->_left->getData()) {
			this->_left = dynamic_cast<AVLTree<int>*>(this->_left)->rotateLeft();
			return this->rotateRight();
		}
		//Right Left Case
		if (balance < -1 && this->_data < this->_right->getData()) {
			this->_right = dynamic_cast<AVLTree<int>*>(this->_right)->rotateRight();
			return this->rotateLeft();
		}
		return this;
	}
	AVLTree<T>* getLeft() const {
		return dynamic_cast<AVLTree<int>*>(this->_left);
	}
	AVLTree<T>* getRight() const {
		return dynamic_cast<AVLTree<int>*>(this->_right);
	}

private:
	int getBalanceFactor() {
		int b = 0;
		if (this->getLeft() != 0) {
			b += this->getLeft()->getHeight();
		}
		if (this->getRight() != 0) {
			b -= this->getRight()->getHeight();
		}
		return b;
	}
	AVLTree<T>* rotateRight() {
		this->_left->setRight(this);
		AVLTree<T>* temp = dynamic_cast<AVLTree<T>*>(this->_left);
		this->setLeft(0);
		return temp;
	}
	AVLTree<T>* rotateLeft() {
		this->_right->setLeft(this);
		AVLTree<T>* temp = dynamic_cast<AVLTree<T>*>(this->_right);
		this->setRight(0);
		return temp;
	}
};