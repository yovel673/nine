#ifndef BSNode_H
#define BSNode_H

#include <exception>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

template<class T>
class BSNode
{
public:
	BSNode(T data) : _data(data), _left(0), _right(0), _parent(0), _count(1)
	{
	}
	BSNode(const BSNode<T>& other)
	{
		*this = other;
	}

	virtual ~BSNode() 
	{
		if (this->_left)
			delete this->_left; 
		if (this->_right)
			delete this->_right; 
	}

	virtual BSNode<T>* insert(T value) 
	{
		if (value < this->_data)
		{
			if (this->_left) 
				this->_left->insert(value); 
			
			else
			{
				this->_left = new BSNode<T>(value); 
				this->_left->setParent(this);
			}
		}
		else if (value> this->_data)
		{
			if (this->_right) 
				this->_right->insert(value);
			else
			{
				this->_right = new BSNode<T>(value); 
				this->_right->setParent(this);
			}
		}
		else 
			this->_count++;

		return this;
	}
	BSNode<T>& operator=(const BSNode<T>& other)
	{
		if (this->_left)
			delete this->_left;
		if (this->_right)

			delete this->_right;
		_data = other._data;
		this->_left = 0;
		this->_right = 0;

		if (other._left)
			this->_left = new BSNode(*other._left);

		if (other._right)
			this->_right = new BSNode(*other._right);
		_count = other._count;
		return *this;
	}
	bool isLeaf() const
	{
		return (this->_left == 0 && this->_right == 0);
	}
	T getData() const 
	{
		return _data;
	}
	virtual BSNode<T>* getLeft() const 
	{
		return this->_left;
	}
	virtual BSNode<T>* getRight() const 
	{
		return this->_right;
	}

	bool search(T val) const 
	{
		bool retVal = false;

		if (_data == val)
			retVal = true;
		else if (_data >= val) 
			if (this->_left)
				retVal = this->_left->search(val); 
		else if (this->_right)
			retVal = this->_right->search(val); 

		return retVal;
	}

	int getHeight() const 
	{
		int retVal;

		if (isLeaf())
			retVal = 1;

		else
		{

			int rightHeight = 0;
			int leftHeight = 0;

			if (this->_left)
				leftHeight = this->_left->getHeight();


			if (this->_right)
				rightHeight = this->_right->getHeight();

			retVal = leftHeight + 1;
			if (rightHeight > leftHeight)
				retVal = rightHeight + 1;

		}

		return retVal;

	}
	int getDepth(const BSNode<T>& root) const 
	{
		if (this == root)
			return 0;
		else if (root->_data >= this->_data)
			if (root->_left)
				return getDepth(root->_left) + 1;
			else if (root->_right)
				return getDepth(root->_right) + 1;
		return -1;
	}

	void setParent(BSNode<T>* parent) 
	{
		this->_parent = parent;
	}
	void setRight(BSNode<T>* right) 
	{
		this->_right = right;
	}
	void setLeft(BSNode<T>* left) 
	{
		left = left;
	}

	void printNodes() const 
	{
		if (this->_left)
			this->_left->printNodes();

		cout << _data << " " << _count << endl;

		if (this->_right)
			this->_right->printNodes();
	}

protected:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;
	BSNode<T>* _parent;

	int _count;

};
#endif