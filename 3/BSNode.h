#ifndef BSNode_H
#define BSNode_H

#include <string>

using namespace std;
template<class t>

class BSNode
{
public:
	BSNode(t data);
	BSNode(const BSNode<t>& other);

	~BSNode();
	
	void insert(t value);
	BSNode<t>& operator=(const BSNode<t>& other);

	bool isLeaf() const;
	t getData() const;
	BSNode<t>* getLeft() const;
	BSNode<t>* getRight() const;

	bool search(t val);

	int getHeight() const;
	int getDepth(BSNode<t>& root) const;

	void printNodes() const;

private:
	t _data;
	BSNode<t>* _left;
	BSNode<t>* _right;

	int _count; //for question 1 part B
};
#endif


template<class t>

BSNode<t>::BSNode(t data) : _data(data), _right(NULL), _left(NULL), _count(1) {}

template<class t>
BSNode<t>::BSNode(const BSNode & other) : _data(other.getData()), _right(other.getRight()), _left(other.getRight()) {}

template<class t>
BSNode<t>::~BSNode()
{
	if (this != nullptr)
	{
		if (this->getLeft() != nullptr)
			delete this->getLeft();
		if (this->getRight() != nullptr)
			delete this->getRight();
	}
	else
		delete this;
}

template<class t>
void  BSNode<t>::insert(t value)
{
	this->search(value);
	if (value < this->getData())
		if (this->getLeft() == NULL)
			this->_left = new BSNode(value);
		else
			this->getLeft()->insert(value);
	else
		if (this->getRight() == NULL)
			this->_right = new BSNode(value);
		else
			this->getRight()->insert(value);

}

template<class t>
BSNode<t> & BSNode<t>::operator=(const BSNode<t> & other)
{
	this->_data = other.getData();
	this->_right = other.getRight();
	this->_left = other.getRight();
	return *this;
}



template<class t>
bool  BSNode<t>::isLeaf() const
{
	return !(this->getLeft() || this->getRight());
}

template<class t>
t  BSNode<t>::getData() const
{
	return this->_data;
}

template<class t>
BSNode<t> * BSNode<t>::getLeft() const
{
	return this->_left;
}


template<class t>
BSNode<t> *  BSNode<t>::getRight() const
{
	return this->_right;
}

template<class t>
bool BSNode<t>::search(t val)
{
	if (this->_data == (val))
	{
		this->_count++;
		return true;
	}
	if (this->_data > val && this->_left != nullptr)
	{
		return this->_left->search(val);
	}
	if (this->_data < val && this->_right != nullptr)
	{
		return this->_right->search(val);
	}
	return false;
}

template<class t>
int  BSNode<t>::getHeight() const
{
	int heightLeft = 0;
	int heightRight = 0;
	if (this->_left)
	{
		heightLeft = this->_left->getHeight();
	}
	if (this->_right)
	{
		heightRight = this->_right->getHeight();
	}
	if (this->isLeaf())
	{
		return 0;
	}
	if (heightLeft > heightRight)
	{
		return (heightLeft + 1);
	}
	return (heightRight + 1);
}

template<class t>
int  BSNode<t>::getDepth(BSNode<t> & root) const
{
	if (!(&root)->search(this->_data))
	{
		return -1;
	}
	if ((&root)->_data == this->_data)
	{
		return 0;
	}
	if ((&root)->_data > this->_data)
	{
		return 1 + this->getDepth(*((&root)->_left));
	}
	if ((&root)->_data < this->_data)
	{
		return 1 + this->getDepth(*((&root)->_right));
	}
	return -1;
}

template<class t>
void  BSNode<t>::printNodes() const
{
	if (this == nullptr)
		return;
	this->getLeft()->printNodes();
	std::cout << this->getData() << " ";
	this->getRight()->printNodes();
}
