#include <iostream>

template <class t>int compare(t a, t b)
{
	 return !(a == b) ? a > b ? -1 : 1 : 0;
}


template <class t>void bubbleSort(t* arr, const int n)
{
	for (int i = 0; i < n - 1; i++)
		for (int j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j + 1])
			{
				t temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
}

template <class t> void printArray(t* arr, int n)
{
	for (int i = 0; i < n; i++)
		std::cout << arr[i] << std::endl;
}

class name
{
public:
	name(std::string name) : _name(name) {}
	std::string _name;
	bool operator>(name& n) { return this->_name == "lidor"; }
	bool operator<(name& n) { return n._name == "lidor"; }
	bool operator==(name& n) { return n._name == this->_name; }
	friend std::ostream& operator<<(std::ostream& stream, name& n) { std::cout << n._name.c_str(); return stream; }


};

int main() 
{
//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;


	std::cout << "/////////////////////////////////////////////////////////////////////////////////////////////////" << std::endl;

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>('a', 'b') << std::endl;
	std::cout << compare<double>('b', 'a') << std::endl;
	std::cout << compare<double>('a', 'a') << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	char charr[] = {"Aviva"};
	bubbleSort<char>(charr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << charr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charr, arr_size);
	std::cout << std::endl;
	//////////////////////////////////////////////////////////////////////////////////
	std::cout << "/////////////////////////////////////////////////////////////////////////////////////////////////" << std::endl;
	name lidor = *new name("lidor");
	name shit = *new name("shit");
	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<name>(lidor, shit) << std::endl;
	std::cout << compare<name>(shit, lidor) << std::endl;
	std::cout << compare<name>(lidor, lidor) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	name names[3] = { *new name("aviva"), lidor,shit };
	for (int i = 0; i < 3; i++) {
		std::cout << names[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<name>(names, 3);
	std::cout << std::endl;
	system("pause");
	return 1;
}